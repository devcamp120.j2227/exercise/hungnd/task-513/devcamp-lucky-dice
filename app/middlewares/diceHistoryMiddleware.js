const getAllDiceHistoryMiddleware = (req, res, next) => {
    console.log("Get All DiceHistory!");
    next();
};

const getDiceHistoryMiddleware = (req, res, next) => {
    console.log("Get a DiceHistory!");
    next();
};

const postDiceHistoryMiddleware = (req, res, next) => {
    console.log("Create a DiceHistory!");
    next();
};

const putDiceHistoryMiddleware = (req, res, next) => {
    console.log("Update a DiceHistory!");
    next();
};

const deleteDiceHistoryMiddleware = (req, res, next) => {
    console.log("Delete a DiceHistory!");
    next();
};
module.exports = {
    getAllDiceHistoryMiddleware,
    getDiceHistoryMiddleware,
    postDiceHistoryMiddleware,
    putDiceHistoryMiddleware,
    deleteDiceHistoryMiddleware
}