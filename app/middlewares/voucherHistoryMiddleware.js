const getAllVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Get All Voucher History!");
    next();
};

const getVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Get a Voucher History!");
    next();
};

const postVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Create a Voucher History!");
    next();
};

const putVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Update a Voucher History!");
    next();
};

const deleteVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Delete a Voucher History!");
    next();
};
module.exports = {
    getAllVoucherHistoryMiddleware,
    getVoucherHistoryMiddleware,
    postVoucherHistoryMiddleware,
    putVoucherHistoryMiddleware,
    deleteVoucherHistoryMiddleware
}