const getAllPrizeHistoryMiddleware = (req, res, next) => {
    console.log("Get All Prize History!");
    next();
};

const getPrizeHistoryMiddleware = (req, res, next) => {
    console.log("Get a Prize History!");
    next();
};

const postPrizeHistoryMiddleware = (req, res, next) => {
    console.log("Create a Prize History!");
    next();
};

const putPrizeHistoryMiddleware = (req, res, next) => {
    console.log("Update a Prize History!");
    next();
};

const deletePrizeHistoryMiddleware = (req, res, next) => {
    console.log("Delete a Prize History!");
    next();
};

module.exports = {
    getAllPrizeHistoryMiddleware,
    getPrizeHistoryMiddleware,
    postPrizeHistoryMiddleware,
    putPrizeHistoryMiddleware,
    deletePrizeHistoryMiddleware
}