// Import thư viện mongoose
const mongoose =  require('mongoose');

// Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Tạo voucherHistorySchema
const voucherHistorySchema = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
	voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
},{
    timestamps: true
});

module.exports = mongoose.model("VoucherHistory", voucherHistorySchema);