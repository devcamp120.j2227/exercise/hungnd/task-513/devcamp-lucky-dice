// Import thư viện mongoose
const mongoose = require("mongoose");

// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance userSchema từ class Schema
const userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
},{
    timestamps: true
});
module.exports = mongoose.model("User", userSchema);
