// Import thư viện mongoose
const mongoose = require("mongoose");

// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance voucherSchema từ class Schema
const voucherSchema = new Schema({
    code: {
        type: String,
        unique: true,
        required: true
    },
	discount: {
        type: Number,
        required: true
    },
	note: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
},{
    timestamps: true
});

module.exports = mongoose.model("Voucher", voucherSchema);