// Import thư viện mongoose
const mongoose = require("mongoose");
// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance diceHistotySchema từ class Schema
const diceHistotySchema = new Schema({
    user: [{
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    }],
    dice: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
},{
    timestamps: true
});
module.exports = mongoose.model("DiceHistory", diceHistotySchema);