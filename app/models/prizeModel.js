// Import thư viện mongoose
const mongoose = require("mongoose");

// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance prizeSchema từ class Schema
const prizeSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    }, 
	description: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
},{
    timestamps: true
});

module.exports = mongoose.model("Prize", prizeSchema);
