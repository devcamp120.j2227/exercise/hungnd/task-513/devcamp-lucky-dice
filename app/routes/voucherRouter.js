// Import thu viện express
const express = require('express');

// Import voucherMiddleware
const voucherMiddleware = require('../middlewares/voucherMiddleware');

// Import voucherController
const voucherController = require('../controllers/voucherController');

// Tạo voucherRouter
const voucherRouter = express.Router();

// Create new voucher
voucherRouter.post('/vouchers', voucherMiddleware.postVoucherMiddleware, voucherController.createVoucher);

// Get all voucher
voucherRouter.get('/vouchers', voucherMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher);

// Get voucher id
voucherRouter.get('/vouchers/:voucherId', voucherMiddleware.getVoucherMiddleware, voucherController.getVoucherById);

// Update voucher id
voucherRouter.put('/vouchers/:voucherId', voucherMiddleware.postVoucherMiddleware, voucherController.updateVoucherById);

// Delete voucher id
voucherRouter.delete('/vouchers/:voucherId', voucherMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucherById);

module.exports = { voucherRouter };