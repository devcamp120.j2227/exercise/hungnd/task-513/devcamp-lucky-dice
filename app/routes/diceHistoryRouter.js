// Khai báo thư viện express
const express = require('express');

// Import diceHistoryMiddleware
const diceHistoryMiddleware = require('../middlewares/diceHistoryMiddleware');

// Import diceHistoryController
const diceHistoryController = require('../controllers/diceHistoryController');

// Tạo diceHistoryRouter 
const diceHistoryRouter = express.Router();

// Create new dice history
diceHistoryRouter.post('/users/:userId/dice-histories', diceHistoryMiddleware.postDiceHistoryMiddleware, diceHistoryController.createDiceHistory);

// Get all dice history
diceHistoryRouter.get('/dice-histories', diceHistoryMiddleware.getAllDiceHistoryMiddleware, diceHistoryController.getAllDiceHistory);

// Get dice history by id
diceHistoryRouter.get('/dice-histories/:diceHistoryId', diceHistoryMiddleware.getDiceHistoryMiddleware, diceHistoryController.getDiceHistoryById);

// Update dice history by id
diceHistoryRouter.put('/dice-histories/:diceHistoryId', diceHistoryMiddleware.putDiceHistoryMiddleware, diceHistoryController.updateDiceHistoryById);

// Delete dice history by id
diceHistoryRouter.delete('/dice-histories/:diceHistoryId', diceHistoryMiddleware.deleteDiceHistoryMiddleware, diceHistoryController.deleteDiceHistoryById);

// Get dice history of user
diceHistoryRouter.get('/devcamp-lucky-dice/dice-history', diceHistoryController.getDiceHistoryOfUser);

module.exports = { diceHistoryRouter };