// Import thu viện express
const express = require('express');

// Tạo diceRouter
const diceRouter = express.Router();

// Import diceController
const diceController = require('../controllers/diceController');

diceRouter.post('/devcamp-lucky-dice/dice', diceController.createDice);

module.exports = { diceRouter };