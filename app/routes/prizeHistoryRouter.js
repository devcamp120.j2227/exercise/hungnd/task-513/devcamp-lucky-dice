// Import thư viện express
const express = require("express");

// Import prizeHistoryMiddleware
const prizeHistoryMiddleware = require('../middlewares/prizeHistoryMiddleware');

// Import prizeHistoryController
const prizeHistoryController = require('../controllers/prizeHistoryController');

// Tạo prizeHistoryRouter
const prizeHistoryRouter = express.Router();

// Create prize history
prizeHistoryRouter.post("/prize-histories", prizeHistoryMiddleware.postPrizeHistoryMiddleware, prizeHistoryController.createPrizeHistory);

// Get all prize history
prizeHistoryRouter.get("/prize-histories", prizeHistoryMiddleware.getAllPrizeHistoryMiddleware, prizeHistoryController.getAllPrizeHistory);

// Get prize history by id
prizeHistoryRouter.get("/prize-histories/:historyId", prizeHistoryMiddleware.getPrizeHistoryMiddleware, prizeHistoryController.getPrizeHistoryById);

// update prize history by id
prizeHistoryRouter.put("/prize-histories/:historyId", prizeHistoryMiddleware.putPrizeHistoryMiddleware, prizeHistoryController.updatePrizeHistoryById);

// Delete prize history by id
prizeHistoryRouter.delete("/prize-histories/:historyId", prizeHistoryMiddleware.deletePrizeHistoryMiddleware, prizeHistoryController.deletePrizeHistoryById);

// Get prize history of user
prizeHistoryRouter.get('/devcamp-lucky-dice/prize-history', prizeHistoryController.getPrizeHistoryOfUser)

module.exports = { prizeHistoryRouter }