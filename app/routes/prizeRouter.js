// Import thu viện express
const express = require('express');

// Import prizeMiddleware
const prizeMiddleware = require('../middlewares/prizeMiddleware');

//Import prizeController
const prizeController = require('../controllers/prizeController');

// Tạo prizeRouter
const prizeRouter = express.Router();

// Create new prize
prizeRouter.post('/prizes', prizeMiddleware.postPrizeMiddleware, prizeController.createPrize);

// get all prize
prizeRouter.get('/prizes', prizeMiddleware.getAllPrizeMiddleware, prizeController.getAllPrize);

//get prize by id
prizeRouter.get('/prizes/:prizeId', prizeMiddleware.getPrizeMiddleware, prizeController.getPrizeById);

// update prize by id
prizeRouter.put('/prizes/:prizeId', prizeMiddleware.putPrizeMiddleware, prizeController.updatePrizeById);

//Delete prize by id
prizeRouter.delete('/prizes/:prizeId', prizeMiddleware.deletePrizeMiddleware, prizeController.deletePrizeById);

module.exports = { prizeRouter }
