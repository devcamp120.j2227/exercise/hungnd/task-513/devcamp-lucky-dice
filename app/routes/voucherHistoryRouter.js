// Import thư viện express
const express = require("express");

// Import voucherHistoryMiddleware
const voucherHistoryMiddleware = require('../middlewares/voucherHistoryMiddleware');

// Import voucherHistoryController
const voucherHistoryController = require('../controllers/voucherHistoryController');

// Tạo voucherHistoryRouter
const voucherHistoryRouter = express.Router();

// Create new voucher history
voucherHistoryRouter.post('/voucher-histories', voucherHistoryMiddleware.postVoucherHistoryMiddleware, voucherHistoryController.createVoucherHistory);

// Get all voucher history
voucherHistoryRouter.get('/voucher-histories', voucherHistoryMiddleware.getAllVoucherHistoryMiddleware, voucherHistoryController.getAllVoucherHistory);

//Get voucher history by id
voucherHistoryRouter.get('/voucher-histories/:historyId', voucherHistoryMiddleware.getVoucherHistoryMiddleware, voucherHistoryController.getVoucherHistoryById);

// Update voucher history by id
voucherHistoryRouter.put('/voucher-histories/:historyId', voucherHistoryMiddleware.putVoucherHistoryMiddleware, voucherHistoryController.updateVoucherHistoryById);

// Delete voucher by id
voucherHistoryRouter.delete('/voucher-histories/:historyId', voucherHistoryMiddleware.deleteVoucherHistoryMiddleware, voucherHistoryController.deleteVoucherHistoryById);

// Get voucher history of user
voucherHistoryRouter.get('/devcamp-lucky-dice/voucher-history',voucherHistoryController.getVoucherHistoryOfUser);

module.exports = { voucherHistoryRouter };