// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Import userModel
const userModel = require("../models/userModel");

// Create new user
const createUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);

    //B2: Kiểm tra dữ liệu
    // Kiểm tra username
    if (!body.username) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "username is not valid!"
        });
    }
    // Kiểm tra firstname
    if (!body.firstname) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "firstname is not valid!"
        });
    }
    // Kiểm tra lastname
    if (!body.lastname) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "lastname is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname
    }
    userModel.create(newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new user successfully!",
                "data": data
            })
        }
    })
};

// Get all user
const getAllUser = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all user
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all user successfully!",
                "data": data
            })
        }
    })
};

// Get user by id
const getUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    userModel.findById(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get user by Id successfully!",
                "data": data
            })
        }
    })
};

// Update user by id
const updateUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra user Id
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // Kiểm tra username
    if (!body.username) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "userName is not valid!"
        });
    }
    // Kiểm tra firstname
    if (!body.firstname) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "firstname is not valid!"
        });
    }
    // Kiểm tra lastname
    if (!body.lastname) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "lastname is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const updateUser = {
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname
    }
    userModel.findByIdAndUpdate(userId, updateUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update user by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete user by id
const deleteUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    userModel.findByIdAndDelete(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete user by Id successfully!",
                "data": data
            })
        }
    })
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}