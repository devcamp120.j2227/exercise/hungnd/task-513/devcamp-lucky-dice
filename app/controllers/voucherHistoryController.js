// Import thư viện express
const mongoose = require("mongoose");
const userModel = require("../models/userModel");

// Import voucherHistoryModel
const voucherHistoryModel = require('../models/voucherHistoryModel');

// Create new Voucher History
const createVoucherHistory = (req, res) => {
    // B1: Thu thập dữ liệu
    const userId = req.body.userId;
    console.log(userId);

    const voucherId = req.body.voucherId;
    console.log(voucherId);

    // B2: Kiểm tra dữ liệu
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    // Kiểm tra voucherId
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "voucher Id không hợp lệ"
        })
    }
    //B3: Thực hiện tạo mới voucher history
    const newVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: userId,
        voucher: voucherId
    }
    voucherHistoryModel.create(newVoucherHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Create new voucher history successfully!",
                data: data
            })
        }
    })
};

// Get all voucher history
const getAllVoucherHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    //thu thập dữ liệu trên front-end
    let userId = req.query.user;

    //tạo ra điều kiện lọc
    let condition = {};

    if (userId) {
        condition.user = userId;
    }
    // xem giá trị của biến đó trước khi đặt vào condition
    console.log(condition); //{ user: '637f567f83b9cfe8d384739a' }

    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all voucher history
    voucherHistoryModel
        .find(condition)
        .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Load all voucher history successfully!",
                data: data
            })
        }
    })
};
 
// Get voucher history by id
const getVoucherHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const historyId = req.params.historyId;
    console.log(historyId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "historyId is not valid!"
        })
    }
    // B3: Thực hiện load voucher history theo id
    voucherHistoryModel.findById(historyId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get voucher history by Id successfully!",
                "data": data
            })
        }
    })
};

// Update voucher by id
const updateVoucherHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const historyId = req.params.historyId;
    console.log(historyId);

    const userId = req.body.userId;
    console.log(userId);

    const voucherId = req.body.voucherId;
    console.log(voucherId);

    // B2: Kiểm tra dữ liệu
    // Kiểm tra historyId
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "historyId không hợp lệ"
        })
    }
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    // Kiểm tra voucherId
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "voucher Id không hợp lệ"
        })
    }
    //B3: Thực hiện tạo update voucher history
    const updateVoucherHistory = {
        user: userId,
        voucher: voucherId
    }
    voucherHistoryModel.create(updateVoucherHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Update voucher history successfully!",
                data: data
            })
        }
    })
};

// Delete voucher history by id
const deleteVoucherHistoryById =  (req, res) => {
    //B1: Thu thập dữ liệu
    const historyId = req.params.historyId;
    console.log(historyId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "historyId is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    voucherHistoryModel.findByIdAndDelete(historyId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete voucher history by Id successfully!",
                "data": data
            })
        }
    })
};

const getVoucherHistoryOfUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const username = req.query.username;
    console.log(username);
    //B2: Kiểm tra dữ liệu
    if (username) {
        userModel.findOne({ username }, (err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                if (!data) {
                    return res.status(500).json({
                        status: " User name is not valid",
                        data: []
                    })
                } else {
                    userModel.find({ username : username })
                    .exec((err, data) => {
                        if (err) {
                            return res.status(500).json({
                                status: " Internal server error",
                                message: err.message
                            })
                        }
                        else {
                            return res.status(200).json({
                                status: `Get voucher history of user successfully`,
                                data: data
                            })
                        }
                    })
                }
            }
        });
    }
    //nếu không truyền vào query username
    voucherHistoryModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all voucher histories successfully",
            data: data
        })
    });
}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById,
    getVoucherHistoryOfUser
}