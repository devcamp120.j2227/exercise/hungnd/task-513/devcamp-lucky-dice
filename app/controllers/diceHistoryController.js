// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Import diceHistoryModel
const diceHistoryModel = require("../models/diceHistoryModel");
const userModel = require("../models/userModel");

// Create new DiceHistory 
const createDiceHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.body.userId;
    console.log(userId);

    const diceResult = Math.floor(6 * Math.random()) + 1;
    console.log(diceResult);

    //B2: Kiểm tra dữ liệu
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    //B3: Thao tác với CSDL
    const newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: userId,
        dice: diceResult
    }
    diceHistoryModel.create(newDiceHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new dice history successfully!",
                "data": data
            })
        }
    })
};

// get all dice history
const getAllDiceHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    //thu thập dữ liệu trên front-end
    let userId = req.query.user;

    //tạo ra điều kiện lọc
    let condition = {};

    if (userId) {
        condition.user = userId;
    }
    // xem giá trị của biến đó trước khi đặt vào condition
    console.log(condition); // { user: '637f567f83b9cfe8d384739a' }

    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all dice history
    diceHistoryModel
        .find(condition)
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    "status": "Error 500: internal server error",
                    "message": err.message
                })
            }
            else {
                return res.status(201).json({
                    "status": "Load all dice history successfully!",
                    "data": data
                })
            }
        })
};

// get dice history by id
const getDiceHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "diceHistoryId is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    diceHistoryModel.findById(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get dice history by Id successfully!",
                "data": data
            })
        }
    })
};

// update dice history by id
const updateDiceHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "diceHistoryId is not valid!"
        })
    }

    if (!body.dice) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Dice is not valid!"
        });
    }
    //B3: Thao tác với CSDL
    const updateDiceHistory = {
        dice: body.dice
    }
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update dice history successfully!",
                "data": data
            })
        }
    })
};

// Delete dice history by id
const deleteDiceHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "diceHistoryId is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete dice history by Id successfully!",
                "data": data
            })
        }
    })
};

// get dice history of user
const getDiceHistoryOfUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const username = req.query.username;
    console.log(username);
    //B2: Kiểm tra dữ liệu
    if (username) {
        userModel.findOne({ username }, (err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                if (!data) {
                    return res.status(500).json({
                        status: " User name is not valid",
                        data: []
                    })
                } else {
                    userModel.find({ username : username })
                    .exec((err, data) => {
                        if (err) {
                            return res.status(500).json({
                                status: " Internal server error",
                                message: err.message
                            })
                        }
                        else {
                            return res.status(200).json({
                                status: `Get Dice history of user successfully`,
                                data: data
                            })
                        }
                    })
                }
            }
        });
    }
    //nếu không truyền vào query username
    diceHistoryModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all dice histories successfully",
            data: data
        })
    });
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById,
    getDiceHistoryOfUser
}