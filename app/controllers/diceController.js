// Import thư viện mongoose
const mongoose = require('mongoose');

const userModel = require('../models/userModel');
const diceHistoryModel = require('../models/diceHistoryModel');
const voucherModel = require('../models/voucherModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');
const prizeModel = require('../models/prizeModel');
const prizeHistoryModel = require('../models/prizeHistoryModel');


// Create new dice
const createDice = (req, res) => {
    //B1: Thu thập dữ liệu
    const username = req.body.username;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;

    console.log(username);

    // Random 1 giá trị xúc xắc bất kỳ
    let diceRandom = Math.floor(Math.random() * 6 + 1);

    //B2: Kiểm tra dữ liệu
    if (!username) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "username is required"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "firstname is required"
        })
    }

    if (!lastname) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "lastname is required"
        })
    }
    //B3: Thực hiện cơ sở dũ liệu
    // Sử dụng userModel tìm kiếm userName
    userModel
        .findOne({ username: username })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            }
            else {
                if (!data) { // Nếu dữ liệu username không có trong hệ thống thì thực hiện tạo mới user
                    userModel.create({
                        _id: mongoose.Types.ObjectId(),
                        username: username,
                        firstname: firstname,
                        lastname: lastname
                    }, (err, dataUser) => {
                        if (err) {
                            return res.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        }
                        else {
                            // Thực hiện tung xúc xắc 1 lần và dùng diceHistoryModel lưu lịch sử tung xúc xắc
                            const newDiceHistory = {
                                _id: mongoose.Types.ObjectId(),
                                user: dataUser._id,
                                dice: diceRandom
                            }
                            diceHistoryModel.create(newDiceHistory, (err, dataDiceHistory) => {
                                if (err) {
                                    return res.status(500).json({
                                        status: "Error 500: Internal server error",
                                        message: err.message
                                    })
                                }
                                else {
                                    if (diceRandom < 3) { // Nếu diceRandom < 3 thì prize và voucher không nhận gì cả
                                        return res.status(200).json({
                                            dice: diceRandom,
                                            prize: null,
                                            voucher: null
                                        })
                                    }
                                    else { // Nếu xúc xắc > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống.
                                        voucherModel.count().exec((err, count) => {

                                            let randomVoucher = Math.floor(Math.random * count);
                                            console.log(count);

                                            voucherModel.findOne().skip(randomVoucher).exec((err, dataVoucher) => {
                                                // Thêm một bản ghi vào VoucherHistory 
                                                const newVoucherHistory = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    user: dataUser._id,
                                                    voucher: dataVoucher._id
                                                }
                                                console.log(newVoucherHistory.voucher);
                                                voucherHistoryModel.create(newVoucherHistory, (err, dataVoucherHistory) => {
                                                    if(err) {
                                                        return res.status(500).json({
                                                            status: "Error 500: Internal server error",
                                                            message: errorCreateVoucherHistory.message
                                                        })
                                                    }
                                                    else {
                                                        // User mới không có prize
                                                        return res.status(200).json({
                                                            dice: dice,
                                                            prize: null,
                                                            voucher: dataVoucherHistory
                                                        })
                                                    }
                                                })
                                            })
                                        })
                                    }
                                }
                            })
                        }
                    })
                }
                else {// Nếu người dùng tồn tại trong hệ thống
                    // Thực hiện tung xúc xắc 1 lần và dùng diceHistoryModel lưu lịch sử tung xúc xắc
                    const newDiceHistory = {
                        _id: mongoose.Types.ObjectId(),
                        user: data._id,
                        dice: diceRandom
                    }
                    diceHistoryModel.create(newDiceHistory, (err, dataDiceHistory) => {
                        if (err) {
                            return res.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        }
                        else {
                            if (diceRandom < 3) { // Nếu diceRandom < 3 thì prize và voucher không nhận gì cả
                                return res.status(200).json({
                                    prize: null,
                                    voucher: null
                                })
                            }
                            else { // Nếu xúc xắc > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống.
                                voucherModel.count().exec((err, count) => {

                                    let randomVoucher = Math.floor(Math.random * count);

                                    voucherModel.findOne().skip(randomVoucher).exec((err, dataVoucher) => {
                                        // Thêm một bản ghi vào VoucherHistory 
                                        const newVoucherHistory = {
                                            _id: mongoose.Types.ObjectId(),
                                            user: data._id,
                                            voucher: dataVoucher._id
                                        }
                                        voucherHistoryModel.create(newVoucherHistory, (err, dataVoucherHistory) => {
                                            if (err) {
                                                return res.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: err.message
                                                })
                                            }
                                            else { // Lần 3 lần xúc xắc gần nhất
                                                diceHistoryModel
                                                    .find()
                                                    .sort({ _id: -1 })
                                                    .limit(3)
                                                    .exec((err, dataDiceHistory) => {
                                                        if (err) {
                                                            return res.status(500).json({
                                                                status: "Error 500: Internal server error",
                                                                message: err.message
                                                            })
                                                        }
                                                        else {
                                                            // Nếu chưa ném đủ 3 lần, không nhận được prize
                                                            if (dataDiceHistory.length < 3) {
                                                                return res.status(200).json({
                                                                    dice: diceRandom,
                                                                    prize: null,
                                                                    voucher: dataVoucher
                                                                })
                                                            }
                                                            else {
                                                                let checkPrize = true;
                                                                dataDiceHistory.forEach(diceHistoryValue => {

                                                                    if (diceHistoryValue.dice < 3) {
                                                                        // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => prize = null
                                                                        checkPrize = false;
                                                                    }
                                                                });
                                                                if (!checkPrize) {
                                                                    return res.status(200).json({
                                                                        dice: diceRandom,
                                                                        prize: null,
                                                                        voucher: dataVoucher
                                                                    })
                                                                }
                                                                else { // Nếu đủ điều kiện 3 lần xúc xắc > 3 thực hiện random prize lấy 1 prize
                                                                    prizeModel.count().exec((err, count) => {

                                                                        let randomPrize = Math.floor(Math.random * count);

                                                                        prizeModel.findOne().skip(randomPrize).exec((err, dataPrize) => {
                                                                            // Lưu vào voucher history
                                                                            const newPrizeHistory = {
                                                                                _id: mongoose.Types.ObjectId(),
                                                                                user: data._id,
                                                                                prize: dataPrize._id
                                                                            }
                                                                            prizeHistoryModel.create(newPrizeHistory, (err, dataPrizeHistory) => {
                                                                                if (err) {
                                                                                    return res.status(500).json({
                                                                                        status: "Error 500: Internal server error",
                                                                                        message: err.message
                                                                                    })
                                                                                }
                                                                                else {
                                                                                    return res.status(200).json({
                                                                                        dice: diceRandom,
                                                                                        prize: dataPrize,
                                                                                        voucher: dataVoucher
                                                                                    })
                                                                                }
                                                                            })
                                                                        })
                                                                    })

                                                                };

                                                            }

                                                        }
                                                    })
                                            }
                                        })
                                    })
                                })
                            }
                        }
                    })
                }
            }
        })
}

module.exports = {
    createDice
}