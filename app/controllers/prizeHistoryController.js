// Import thư viện express
const mongoose = require("mongoose");

// Import prizeHistoryModel
const prizeHistoryModel = require('../models/prizeHistoryModel');
const prizeModel = require("../models/prizeModel");
const userModel = require("../models/userModel");
const { updateUserById } = require('./userController');
const { updatePrizeById } = require('./prizeController');

// Create new prize history
const createPrizeHistory = (req, res) => {
    // B1: Thu thập dữ liệu
    const userId = req.body.userId;
    console.log(userId);

    const prizeId = req.body.prizeId;
    console.log(prizeId);

    // B2: Kiểm tra dữ liệu
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    // Kiểm tra prizeId
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Prize Id không hợp lệ"
        })
    }
    //B3: Thực hiện tạo mới prize history
    const newPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: userId,
        prize: prizeId
    }
    prizeHistoryModel.create(newPrizeHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new prize history successfully!",
                "data": data
            })
        }
    })
};

// Get all prize history
const getAllPrizeHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    //thu thập dữ liệu trên front-end
    let userId = req.query.user;

    //tạo ra điều kiện lọc
    let condition = {};

    if (userId) {
        condition.user = userId;
    }
    // xem giá trị của biến đó trước khi đặt vào condition
    console.log(condition); //{ user: '637f568e83b9cfe8d384739d' }

    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all prize history
    prizeHistoryModel
        .find(condition)
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: internal server error",
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    status: "Load all prize history successfully!",
                    data: data
                })
            }
        })
};

//Get prize history by id
const getPrizeHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const historyId = req.params.historyId;
    console.log(historyId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "historyId is not valid!"
        })
    }
    //B3: Thực hiện load prize history theo id
    prizeHistoryModel.findById(historyId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get prize history by Id successfully!",
                "data": data
            })
        }
    })
};

// update prize history by id
const updatePrizeHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const historyId = req.params.historyId;
    console.log(historyId);

    const userId = req.body.userId;
    console.log(userId);

    const prizeId = req.body.prizeId;
    console.log(prizeId);
    //B2: Kiểm tra dữ liệu
    // KIểm tra historyId
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "historyId is not valid!"
        })
    }
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    // Kiểm tra prizeId
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Prize Id không hợp lệ"
        })
    }
    //B3: Thực hiện khởi update prize history
    const updatePrizeHistory = {
        user: userId,
        prize: prizeId
    }
    prizeHistoryModel.findByIdAndUpdate(historyId, updatePrizeHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update prize history by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete prize history by id
const deletePrizeHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const historyId = req.params.historyId;
    console.log(historyId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "historyId is not valid!"
        })
    }
    // B3: Thực hiện xóa prize history theo id
    prizeHistoryModel.findByIdAndDelete(historyId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete prize history by Id successfully!",
                "data": data
            })
        }
    })
};

// get prize history of user
const getPrizeHistoryOfUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const username = req.query.username;
    console.log(username);

    //B2: Kiểm tra dữ liệu
    if (username) {
        userModel.findOne({ username }, (err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                if (!data) {
                    return res.status(500).json({
                        status: " User name is not valid",
                        data: []
                    })
                } else {
                    userModel.findOne({ username }, (err, data) => {
                        if (err) {
                            return res.status(500).json({
                                status: " Internal server error",
                                message: err.message
                            })
                        }
                        else {
                            return res.status(200).json({
                                status: `Get prize history of user successfully`,
                                prizehistory: data.prizehistory
                            })
                        }
                    })
                }
            }
        });
    }
    //nếu không truyền vào query username
    prizeHistoryModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all prize histories successfully",
            data: data
        })
    });
}

module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
    getPrizeHistoryOfUser
}