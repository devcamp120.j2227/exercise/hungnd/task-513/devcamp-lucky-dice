// Import thư viện mongoose
const mongoose = require('mongoose');

//Import voucherModel
const voucherModel = require('../models/voucherModel');

// Create new voucher
const createVoucher = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);

    //B2: Kiểm tra dữ liệu
    // Kiểm tra code
    if (!body.code) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Code is not valid!"
        });
    }
    // Kiểm tra discount
    if (!body.discount) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "discount is not valid!"
        });
    }
    // Kiểm tra note
    if (!body.note) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "note is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới voucher
    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note
    }
    voucherModel.create(newVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new voucher successfully!",
                "data": data
            })
        }
    })
};

//Get all voucher
const getAllVoucher = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all voucher
    voucherModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Load all voucher successfully!",
                data: data
            })
        }
    })
};

// Get voucher by id
const getVoucherById = (req, res) => {
    //B1: Thu thập dữ liệu
    const voucherId = req.params.voucherId;
    console.log(voucherId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "voucherId is not valid!"
        })
    }
    // B3: Thực hiện load voucher theo id
    voucherModel.findById(voucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get voucher by Id successfully!",
                "data": data
            })
        }
    })
};

// Update voucher by id
const updateVoucherById = (req, res) => {
    //B1: Thu thập dữ liệu
    const voucherId = req.params.voucherId;
    console.log(voucherId);

    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra user Id
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "voucherId is not valid!"
        })
    }
    // Kiểm tra code
    if (!body.code) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Code is not valid!"
        });
    }
    // Kiểm tra discount
    if (!body.discount) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "discount is not valid!"
        });
    }
    // Kiểm tra note
    if (!body.note) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "note is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const updateVoucher = {
        code: body.code,
        discount: body.discount,
        note: body.note
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update voucher by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete voucher by id
const deleteVoucherById = (req, res) => {
    //B1: Thu thập dữ liệu
    const voucherId = req.params.voucherId;
    console.log(voucherId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "voucherId is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    voucherModel.findByIdAndDelete(voucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete voucher by Id successfully!",
                "data": data
            })
        }
    })
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}