// Import thư viện expressjs tương đương import express from "express";
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Import thư viện path
const path = require('path');

// Import thư viện mongoose
const mongoose = require('mongoose');

// khai báo cổng chạy project
const port = 8000;

const randtoken = require('rand-token');

// Khai báo để sử dụng bodyJson
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Import timeCurrentMiddle
const timeCurrentMiddleware = require('./app/middlewares/timeCurrentMiddleware');
app.use('/', timeCurrentMiddleware.getTimeCurrent);

// Show method request
app.use((req, res, next) => {
    console.log("Request method: ", req.method);
    next();
})

// Import randomRouter
const { randomRouter } = require('./app/routes/randomRouter');
app.use('/', randomRouter);

// Import userRouter và sử dung
const { userRouter } = require('./app/routes/userRouter');
app.use('/', userRouter);

//Import diceHistoryRouter và sử dung
const { diceHistoryRouter } = require('./app/routes/diceHistoryRouter');
app.use('/', diceHistoryRouter);

// Import prizeRouter
const { prizeRouter } = require("./app/routes/prizeRouter");
app.use('/', prizeRouter);

// Import voucherRouter
const { voucherRouter } = require('./app/routes/voucherRouter');
app.use('/', voucherRouter);

//Import prizeHistoryRouter
const { prizeHistoryRouter } = require('./app/routes/prizeHistoryRouter');
app.use('/', prizeHistoryRouter);

// Import voucherHistoryRouter
const { voucherHistoryRouter } = require('./app/routes/voucherHistoryRouter');
app.use('/', voucherHistoryRouter);

// Import diceRouter
const { diceRouter } = require('./app/routes/diceRouter');
app.use('/', diceRouter);

// Show mockup
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + "/views/Lucky Dice Casino.html"));
});

// Khai báo để sử dụng tài nguyên tĩnh
app.use(express.static(__dirname + "/views"));

// Kết nối với mongo
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDice", (error) => {
    if (error) throw error;
    console.log('✅Successfully connected');
});
app.listen(port, () => {
    console.log("🟢App listening on port: ", port);
});